import tweepy
import numpy
import scipy
from tweepy import Stream
from tweepy import StreamListener
from  textblob import TextBlob
import re
import csv
import nltk
import pandas as pd
from collections import Counter
from datetime import datetime, date, time, timedelta
import json
import os
import io
import re
import time
import pickle
import pymongo
from pymongo import MongoClient
import matplotlib.pyplot as plt
import geopy
from geopy.geocoders import Nominatim
from geopy.extra.rate_limiter import RateLimiter
import spacy
nlp = spacy.load('en_core_web_sm')
from collections import Counter
import copy
import time
from  textblob import TextBlob
import itertools, collections
import string
from vaderSentiment.vaderSentiment import SentimentIntensityAnalyzer
analyser = SentimentIntensityAnalyzer()
import nltk
from nltk import bigrams
from nltk.corpus import stopwords
nltk.download('stopwords')
stop_words = set(stopwords.words('english'))

# Import personal API login & initiate the connection to Twitter Rest API
# !!! This requires to have in the same folder of this script a secrets.py file
from .secrets import my_API, MONGO_CONNECTION
from .parameters import *

KDB_PATH = '../knowledgeDB/'

def connect_tweepy_api(consumer_key=my_API[2], consumer_secret=my_API[3], 
                       access_token=my_API[0], access_token_secret=my_API[1]):
    
    auth = tweepy.OAuthHandler(consumer_key, consumer_secret)
    auth.set_access_token(access_token, access_token_secret)
    api = tweepy.API(auth, # monitor remaining calls and block until replenished  
                    retry_count=3,retry_delay=5,retry_errors=set([401, 404, 500, 503]),
                    wait_on_rate_limit=True)
    
    return api, auth



########### Streaming API class
class StreamListener(tweepy.StreamListener):
    '''
    This class is used to stream tweets (no retweets), save them in a mongo db
    adds vader sentiment polarity for each tweet-db-entry
    '''
    
    def __init__(self, time_limit=60,tweet_limit=50000):
        self.start_time = time.time()
        self.limit = time_limit
        self.t_limit = tweet_limit
        self.tweets = 0
        super(StreamListener, self).__init__()
        
    def twitter_time_to_object(self, time_string):
        
        twitter_format = "%a %b %d %H:%M:%S %Y"
        match_expression = "^(.+)\s(\+[0-9][0-9][0-9][0-9])\s([0-9][0-9][0-9][0-9])$"
        match = re.search(match_expression, time_string)
        date_object = None
        if match is not None:
            first_bit = match.group(1)
            second_bit = match.group(2)
            last_bit = match.group(3)
            new_string = first_bit + " " + last_bit
            date_object = datetime.strptime(new_string, twitter_format)
        
        return date_object
    
    def resolve_full_text(self, status_json):
        '''
        If field full_text exists, copy the full text in the 'text' field for easier
        use in downstream analysis
        '''
        if hasattr(status_json, 'extended_tweet'):
            status_json['text'] = status_json['extended_tweet']['full_text']
        
        return status_json
        
        
    def create_db_entry(self, status_json):
    
        user_info_to_keep = ["id_str", "name","screen_name","location","description","url",
        "protected", "followers_count", "friends_count","created_at",
        "favourites_count","utc_offset", "time_zone","geo_enabled",
        "verified", "statuses_count", "lang"]
    
        status_info_to_keep = ["id_str","created_at", "in_reply_to_status_id_str", "text","in_reply_to_screen_name",
                       "in_reply_to_user_id_str", "geo","coordinates","place","extended_tweet",
                      "reply_count","retweet_count","favorite_count","entities","lang"]
        
        status_json = self.resolve_full_text(status_json)
        status_json['created_at'] = self.twitter_time_to_object(status_json['created_at'])
        status_json['user']['created_at'] = self.twitter_time_to_object(status_json['user']['created_at'])
        db_entry = {k: status_json.get(k,None) for k in status_info_to_keep}
        db_entry['user'] = {k: status_json['user'].get(k,None) for k in user_info_to_keep}
        
        # add vader-sentiment polarity
        db_entry['polarity'] = analyser.polarity_scores(status_json['text'])['compound']
    
        return db_entry

    def on_status(self, status):
        
        ## Connect mongo db
        client = MongoClient(MONGO_CONNECTION)
        db = client['tweets']
        
        if ((time.time() - self.start_time) < self.limit) & (self.tweets < self.t_limit):
            # exclude retweets
            if (hasattr(status, 'retweeted_status')) is False:
                #Store the Tweet data in the defined MongoDB collection
                db_entry = self.create_db_entry(status._json)
                db['tweetStream'].insert_one(db_entry)
                self.tweets += 1

            return True
        
        else:
            print(f"Run for {time.time() - self.start_time} seconds and archived {self.tweets} tweets")
            return False

    def on_error(self, status_code):
        if status_code == 420: # 420 error is rate-limited error
            print(f'Error Status code {status_code}')
            return False #returning False in on_data disconnects the stream


        
def run_streaming(api, filter_params, max_time=60*14, max_tweets=50000):
    '''
    Function for let the streaming running for defined time or number of tweets
    by default, it runs for 14 mins or until 50K tweets are collected
    
    Args:
    api : tweepy api
    filter_params: dict with tweepy filters as keys
    max_time: int, max time in seconds
    max_tweet: int, max number of tweets to collect before stopping the stream
    '''
    
    stream_listener = StreamListener(time_limit=60*1,tweet_limit=100) # stream for 15 minutes, or until 50k tweet
    stream = tweepy.Stream(auth=api.auth, listener=stream_listener) # connect stream
    stream.filter(languages= filter_params['languages'],
                  track= filter_params['track'],
                  locations = filter_params['locations'],
                  follow = filter_params['follows'])
    
######### Search API crwaler   
def tweets_search_crawler(api, query, languages, time_limit, tweets_limit, db):
    
    n_tweets = 0
    start_time = time.time()

    while (((time.time()-start_time)<time_limit) & (n_tweets < tweets_limit)):
    
        try:
            for status in tweepy.Cursor(api.search,  q = query, lang = languages,
                            result_type = 'mixed',include_entities=True,tweet_mode="extended",
                               wait_on_rate_limit=True).items():
                db_entry = create_db_entry(status._json)
                db.tweetSearch.insert_one(db_entry)
                n_tweets+=1
                print('collected: ' + str(n_tweets) + ' tweets')
        except tweepy.TweepError as e:
            print(e)
            time.sleep(60*15)
            continue
        except StopIteration:
            break
    print(f"Run for {time.time() - start_time} seconds and archived {n_tweets} tweets")  
    
#############################################


def get_trends_dict(place, api, geolocator = None):
    """ Get trends of a given place
    
    Args:
        place (str): a name of a city or country
        
    Returns:
        a dictionary with fields 
        'location' location the trends refer to
        'created_at' when the oldest trend started trending
        'as_of' timestamp when the list of trends was created
        'trends' a dict with 'name','tweet_volume' (volume of the trend, if available - unstable)
        'is_hashtag' (boolean),'named_entity'(extracted by Spacy, if available)

    """
    if geolocator is None:
        # instanciate geolocator 
        geolocator = Nominatim(user_agent="STAMINA-h2020")
        
    trends_dict = {}
    location = geolocator.geocode(place)
    closest_place = api.trends_closest(location.latitude, location.longitude)
    closest_place[0].pop('url')
    trends = api.trends_place(id=closest_place[0]['woeid'])
    trends_dict['location'] = closest_place[0]
    trends_dict['location']['place_original'] = place
    trends_dict['created_at'] = trends[0]['created_at']
    trends_dict['as_of'] = trends[0]['as_of']
    for t in trends[0]['trends']:
        t.pop('url')
        t.pop('promoted_content')
        t.pop('query')
        t['is_hashtag'] = t['name'].startswith('#')
        t['named_entity'] =  [ent.label_ for ent in nlp(t['name']).ents]
    trends_dict['trends'] = trends[0]['trends']
        
    return trends_dict

############ functions to create queries
def get_disease_keywords(disease):
    '''
    Function that, given a STAMINA-disease name returns related keywords
    or general health-keywords if disease name is not in the KDB
    '''
    
    if disease in diseases:
        disease = ''.join(x.lower() for x in disease if x.isalpha())
        ## Load keywords to track
        keywords = open( KDB_PATH + 'keywords_' + disease + '.txt', 'r')
        keywords = keywords.read()
        keywords_list = keywords.split(",\n")
    else:
        keywords_list = open(KDB_PATH + 'keywords_health_broniatowski.txt','r')
        keywords = keywords.read()
        keywords_list = keywords.split(",\n")
    
    return keywords_list


def bounding_boxes_list(locations):
    
    '''
    Given a name of place, returns a bounding box of the place in this order
    [city_one_SW_longitude, city_one_SW_latitude, 
    city_one_NE_longitude, city_one_NE_latitude,
    city_two_SW_longitude,...]
    '''
    
    all_boxes = []
    if len(locations)>0:
        geolocator = Nominatim(user_agent="STAMINA-h2020")
        for l in locations:
            box = geolocator.geocode(l).raw['boundingbox']
            # re-order coords according to twitter-required order
            box_ordered = [box[i] for i in [2,0,3,1]]
            all_boxes.append(box_ordered)
        all_boxes = [float(item) for sublist in all_boxes for item in sublist]

    return all_boxes


def generate_streaming_filter_query(disease_keywords, input_keywords):
    '''
    Given list of user inputted keywords and disease-related keywords, create
    the query (parameter 'track' in Tweepy streaming class)
    Args:
    disease_keywords: list of keywords related to a disease or health-related words if no disease specified
    input_keywords: list of keywords provided by user or empty list
    Returns:
    List of expressions to use as filter in tweeter Streaming API
    '''
    if len(input_keywords)>0:
        query_list = []
        for dk in disease_keywords:
            for ik in input_keywords:
                query_list.append(dk +' ' + ik)
    else:
        query_list = disease_keywords

    return query_list


def get_accounts_to_monitor():
    
    '''
    Get the list of user ids to follow as found in the KDB (list is taken from epitwittr R package)
    Accounts to monitor could be provided by user...?
    '''
    users_ids_list = None
    PATH = KDB_PATH + 'users_to_monitor.csv'
    if path.exists(PATH):
        users_to_track = pd.read_csv(PATH , index_col = 0)
        users_ids_list = [str(i) for i in users_to_track.id_str.values]
    
    
    return users_ids_list


def stream_filter(keywords, locations, disease, languages, follows):
    '''
    Given keywords, locations, disease, languages provided by user, create the dict
    with filters to pass to the streaming class
    follows : True if we want to stream tweets from specific users defined in the knowledge db
    '''
    
    dis_k = get_disease_keywords(disease)
    
    stream_filter = dict( (k,None) for k in ['track','languages','locations'])
    stream_filter['track'] =  generate_streaming_filter_query(dis_k, keywords)
    stream_filter['languages'] = languages
    stream_filter['locations'] = bounding_boxes_list(locations)
    if follows:
        stream_filter['follows'] =  get_accounts_to_monitor()
    else:
        stream_filter['follows'] = None
        
    return stream_filter

def generate_search_api_query(dis_k, input_keywords, no_retweets = True):
    '''
    Given the list of disease-related keywords and the user inputted keywords
    creates a query string of less than 500 chars
    '''
    
    
    user_k_str = ' OR '.join([k for k in input_keywords])
    tmp_len = len(user_k_str)
    dis_k_str = dis_k[0]
    tmp_len = len(user_k_str) + len(dis_k_str)
    # add keywords only as long as total is below 450 (I left 50 chars of margin)
    for k in dis_k[1:]:
        if (tmp_len < 450):
            dis_k_str = dis_k_str + ' OR ' + k
            tmp_len = len(dis_k_str)
    
    q_string = '('+ dis_k_str + ')' + ' ('+ user_k_str + ')'
    
    if no_retweets:
        q_string = q_string + " -filter:retweets"
    
    return q_string


def generate_search_api_simple_query(dis_k, input_keywords, no_retweets = True):
    '''
    Given the list of disease-related keywords and the user inputted keywords
    creates a query string of less than 100 chars, all words in OR
    '''
    
    
    user_k_str = ' OR '.join([k for k in input_keywords])
    tmp_len = len(user_k_str)
    dis_k_str = dis_k[0]
    tmp_len = len(user_k_str) + len(dis_k_str)
    # add keywords only as long as total is below 450 (I left 50 chars of margin)
    for k in dis_k[1:]:
        if (tmp_len < 100):
            dis_k_str = dis_k_str + ' OR ' + k
            tmp_len = len(dis_k_str)
    
    q_string = dis_k_str + ' OR ' + user_k_str 
    
    if no_retweets:
        q_string = q_string + " -filter:retweets"
    
    return q_string




############## tweets to db utils

def create_db_entry(status_json):
    
        user_info_to_keep = ["id_str", "name","screen_name","location","description","url",
        "protected", "followers_count", "friends_count","created_at",
        "favourites_count","utc_offset", "time_zone","geo_enabled",
        "verified", "statuses_count", "lang"]
    
        status_info_to_keep = ["id_str","created_at", "in_reply_to_status_id_str", "text","in_reply_to_screen_name",
                       "in_reply_to_user_id_str", "geo","coordinates","place","extended_tweet",
                      "reply_count","retweet_count","favorite_count","entities","lang"]
    
        status_json['created_at'] = twitter_time_to_object(status_json['created_at'])
        status_json['user']['created_at'] = twitter_time_to_object(status_json['user']['created_at'])
        db_entry = {k: status_json.get(k,None) for k in status_info_to_keep}
        db_entry['user'] = {k: status_json['user'].get(k,None) for k in user_info_to_keep}
    
        return db_entry
    
def twitter_time_to_object(time_string):
        
        twitter_format = "%a %b %d %H:%M:%S %Y"
        match_expression = "^(.+)\s(\+[0-9][0-9][0-9][0-9])\s([0-9][0-9][0-9][0-9])$"
        match = re.search(match_expression, time_string)
        date_object = None
        if match is not None:
            first_bit = match.group(1)
            second_bit = match.group(2)
            last_bit = match.group(3)
            new_string = first_bit + " " + last_bit
            date_object = datetime.strptime(new_string, twitter_format)
        
        return date_object
