import tweepy
import json
import csv
import time
import requests

from IPython.display import clear_output

# Import personal API login & initiate the connection to Twitter Rest API
# !!! This requires to have in the same folder of this script a Tri_API_key.py file

from .API_key import my_API
from .API_key import BEARER_TOKEN
#oauth = OAuth(my_API[0], my_API[1], my_API[2], my_API[3])
#twitter = Twitter(auth=oauth, retry=True)

TWEET_FIELDS = ['context_annotations', 
                'created_at',
                'conversation_id',
                'entities',
                'in_reply_to_user_id',
                'lang',
                'public_metrics',
                'source',
                'reply_settings',
                'referenced_tweets',
                'author_id']

USER_FIELDS = ['created_at',
               'description',
               'location',
               'profile_image_url',
               'protected',
               'public_metrics',
               'url',
               'verified']

def connect_full_client(consumer_key=my_API[2], consumer_secret=my_API[3], 
                       access_token=my_API[0], access_token_secret=my_API[1]):
    
    client = tweepy.Client(bearer_token=BEARER_TOKEN, 
                           consumer_key=consumer_key, 
                           consumer_secret=consumer_secret, 
                           access_token=access_token, 
                           access_token_secret=access_token_secret,
                           wait_on_rate_limit=True)
    return client

def connect_quick_client():
    
    client = tweepy.Client(bearer_token=BEARER_TOKEN,wait_on_rate_limit=True)
    
    return client

def get_user(client,handle):
    try:
        resp = client.get_user(username=handle,user_fields=USER_FIELDS)
        return resp.data.data
    except:
        return None

def get_user_id(client,user_id):
    try:
        resp = client.get_user(id=user_id,user_fields=USER_FIELDS)
        return resp.data.data
    except:
        return None
    
def get_users_ids(client,account_list):
    full_accs = []
    acc_count = len(account_list)
    for i in range(int(acc_count / 100) + 1):
        resp = client.get_users(ids=account_list[i*100:min((i+1)*100, acc_count)],user_fields=USER_FIELDS)
        full_accs.extend([obj.data for obj in resp.data])
        if i <= 5 or (i < 100 and i % 10 == 0) or i % 100 == 0:
            print(f"{i} batches processed")
       
    return full_accs
    
def get_timeline(client,user_id,tweets_limit=3200,excl=['retweets']):
    
    tweets = []
    
    for tweet in tweepy.Paginator(client.get_users_tweets,
                                  id=user_id,
                                  tweet_fields=TWEET_FIELDS,
                                  exclude=excl,
                                  max_results=100).flatten(limit=tweets_limit):
        tweets.append(tweet.data)
        
    return tweets

def get_follower_ids(client,user_id):
    data=[]
    
    for response in tweepy.Paginator(client.get_users_followers, 
                                     id=user_id,
                                     max_results=1000,
                                     limit=999).flatten():
        data.append(response)
        
    follower_ids = [item.id for item in data]
    
    return follower_ids

def get_following_ids(client,user_id):
    
    data=[]
    
    for response in tweepy.Paginator(client.get_users_following, 
                                     id=user_id,
                                     max_results=1000,
                                     limit=999).flatten():
        data.append(response)
        
    following_ids = [item.id for item in data]
    
    return following_ids

def search_7_days(client,query,tweets_limit=5000):
    
    tweets = []
    
    for tweet in tweepy.Paginator(client.search_recent_tweets, 
                                  query=query, 
                                  tweet_fields=TWEET_FIELDS, 
                                  max_results=100).flatten(limit=tweets_limit):
        tweets.append(tweet.data)
        
    return tweets