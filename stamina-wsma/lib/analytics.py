import tweepy
import numpy
import scipy
from tweepy import Stream
from tweepy import StreamListener
from  textblob import TextBlob
import re
import csv
import numpy as np
import scipy
import nltk
import pandas as pd
from collections import Counter
from datetime import datetime, date, time, timedelta
import json
import os
import io
import re
import time
import pickle
import pymongo
from pymongo import MongoClient
import matplotlib.pyplot as plt
import geopy
from geopy.geocoders import Nominatim
from geopy.extra.rate_limiter import RateLimiter
import spacy
nlp = spacy.load('en_core_web_sm')
from collections import Counter
import copy
import time
from  textblob import TextBlob
import itertools, collections
import string
import sys
from .parameters import *
from vaderSentiment.vaderSentiment import SentimentIntensityAnalyzer
analyser = SentimentIntensityAnalyzer()
import nltk
from nltk import bigrams
from nltk.corpus import stopwords
nltk.download('stopwords')
stop_words = set(stopwords.words('english'))

KDB_PATH = '../knowledgeDB/'

def search_words_in_tweet(tweet, target_words_list):
    '''
    Given a tweet in the shape of a dict and a list of words, search for these words
    in the tweet-dict
    Args:
        tweet: dict
        target_words_list: list
    Returns:
        an empty list if no matching word is found, a list of matching word otherwise
    '''
    k_match = []
    if tweet['extended_tweet'] is not None:
        k_match = [ k for k in target_words_list if k in tweet['extended_tweet']['full_text']]
        if len(tweet['extended_tweet']['entities']['urls'])>0:
            k_match_url = [k for k in target_words_list if k in tweet['extended_tweet']['entities']['urls'][0]['expanded_url']]
            k_match = k_match + k_match_url
                
    elif tweet['text'] is not None:
        k_match = [ k for k in target_words_list if k in tweet['text']]
            
    elif len(tweet['entities']['urls'])>0:
        tmp = tweet['entities']['urls'][0]['expanded_url']
        k_match = k_match + [k for k in target_words_list if k in tmp]
        
    # remove duplicates 
    if len(k_match)>0:
        k_match = list(set(k_match))
    
    return k_match


def summarise_keywords_counts(keywords_counts, disease_words, disease_name):
    '''
    Given a dict with keywords and related occurences, the words related to the user-inputted
    disease and the user-inputted disease name, return a dict where all the disease-related words counts are
    summarised in one key-value pair
    '''
    
    disease_sum = 0
    for k in disease_words:
        disease_sum = disease_sum + keywords_counts[k]
        keywords_counts.pop(k)
    if disease_name is None:
        disease_name = "health_related_words"
        
    keywords_counts[disease_name] = disease_sum
    
    return keywords_counts

def get_min_max_date_tweets(cursor):
    '''
    Args:
    cursor: cursor to mongo collection
    
    returns:
    time min: datetime, date of oldest tweet in the collection
    time max: datetime, date of newest tweet in the collection
    '''
    
    time_min = 0
    for c in cursor.find().sort('created_at', pymongo.ASCENDING).limit(1):
        time_min = c['created_at']
    time_max = 0
    for c in cursor.find().sort('created_at', pymongo.DESCENDING).limit(1):
        time_max = c['created_at']
    
    return time_min, time_max

def get_disease_keywords(disease):
    '''
    Function that, given a STAMINA-disease name returns related keywords
    or general health-keywords if disease name is not in the KDB
    '''
    
    if disease in diseases:
        disease = ''.join(x.lower() for x in disease if x.isalpha())
        ## Load keywords to track
        keywords = open( KDB_PATH + 'keywords_' + disease + '.txt', 'r')
        keywords = keywords.read()
        keywords_list = keywords.split(",\n")
    else:
        keywords_list = open(KDB_PATH + 'keywords_health_broniatowski.txt','r')
        keywords = keywords.read()
        keywords_list = keywords.split(",\n")
    
    return keywords_list

def aggregate_stats(db,tweets_collection, keywords, disease_name = None, start_time=None, 
                              end_time=None, freq_days=0, freq_hours=0, freq_mins=0):
    
    '''
    Aggregate tweets over defined periods and compute some stats. If no start and end time provided,
    all tweets from the collection are retrieved.
    
    Args:
    db : mongo database object
    tweets_collection: name of collection with tweets
    keywords: list of words
    disease_name: one of STAMINA diseases or None
    start_time: datetime, date from which aggregation start or None
    end_time: datetime, date until which aggregation is done or None
    freq_days: int with num of days indicating length of aggregation window or 0 
    freq_hours: int with num of hrs indicating length of aggregation window or 0 
    freq_mins: int with num of mins indicating length of aggregation window or 0 
    
    Returns:
    nothing. Creates or updates a collection where aggregated stats are stored
    '''
        
    # get cursor to tweets collection
    cursor = db[tweets_collection]
    
    if (start_time is None) and (end_time is None):
        start_time, end_time = get_min_max_date_tweets(cursor)
    
    if freq_days>0:
        collection = 'days_aggregate_d' + str(freq_days)
    if freq_hours>0:
        collection = 'hours_aggregate_h'+ str(freq_hours)
    if freq_mins>0:
        collection = 'minutes_aggregate_m'+ str(freq_mins)
        
    # crate collection to save aggr results
    coll = db[collection]
    
    # make list of all words to look for
    disease_keywords = get_disease_keywords(disease_name)
    keywords = keywords + disease_keywords
        
    # create collection with aggr stats, one collection per frequency
    time_freq = timedelta(days = freq_days, hours = freq_hours, minutes = freq_mins)
    n_loops = np.int(np.ceil((end_time - start_time)/time_freq))
    k = []
    for d in range(n_loops):
        end_time = start_time + time_freq
        cursor_obj = cursor.find( {'created_at': {'$lt': end_time, '$gte': start_time}})
        
        document = {}
        document['start_time'] = start_time
        document['end_time'] = end_time
        keywords_counts = dict((k,0) for k in keywords)
        sentiment = dict((k, None) for k in ['mean','std','skew'])
        sent_all = []
        for t in cursor_obj:
            k_match = search_words_in_tweet(t, keywords_counts.keys())
            if len(k_match)>0:
                if (t['text'] is not None):
                    sent_all.append(analyser.polarity_scores(t['text'])['compound'])
                for k in k_match:
                    keywords_counts[k] = keywords_counts[k] + 1
        
        sentiment['mean'] = np.mean(sent_all)
        sentiment['std'] = np.std(sent_all)
        sentiment['skew'] = scipy.stats.skew(sent_all)
        
        if (len(disease_keywords)>0):
            keywords_counts = summarise_keywords_counts(keywords_counts, disease_keywords,disease_name)
        
      
        # make entry for db
        document['word_counts'] = keywords_counts
        document['sentiment'] = sentiment
        coll.insert_one(document)
        
        start_time = end_time

    
def remove_url(txt):

    url_pattern = re.compile(r'https?://\S+|www\.\S+')
    no_url = url_pattern.sub(r'', txt)

    return no_url


def filter_bigrams(words_to_keep, bigrams):
    '''
    Given a bigram and a list of words, keep only the tuple of the bigrams
    containing one of those words
    '''

    words_to_keep = [''.join([i for i in s if not i.isdigit()]) for s in words_to_keep]
    words_to_keep = [re.sub(r'(@[A-Za-z0-9]+)|([^0-9A-Za-z \t])|(\w+:\/\/\S+)', r' ', word) for word in words_to_keep]
    words_to_keep = [word.lower().split() for word in words_to_keep]
    words_to_keep =  list(itertools.chain(* words_to_keep))#list(set([item for sublist in words_to_keep for item in sublist]))
    bigram_filt = [p for p in bigrams if (p[0] in words_to_keep) or (p[1] in words_to_keep)]
    
    return bigram_filt


def get_bigrams(cursor, start_time=None, end_time=None, keywords=None):
    '''
    Create the bigrams from the tweet collection
    
    Args:
    cursor: a mongo cursor to a tweet collection
    start_time: datetime or None, tweets older than this time are retrieved
    end_time: datetime or None, tweets up to this time are retrieved
    keywords: list or None, list of words to filter the bigrams with
    
    Returns:
    bigrams tuples and respective counts, ordered from most to least common
    '''
    
    if (start_time is not None) and (end_time is not None):
         cursor_obj = cursor.find( {'created_at': {'$lt': end_time, '$gte': start_time}})
    else:
        cursor_obj = cursor.find()
    
    tw_list = []
    for tw in cursor.find():
        tw_list.append(tw['text'])  
        
    # remove empty tweets
    tw_list = [t for t in tw_list if t is not None]
    # remove URLs
    tweets_no_urls = [remove_url(tweet) for tweet in tw_list]
    # remove numbers
    tweets_no_numbers = [''.join([i for i in s if not i.isdigit()]) for s in tweets_no_urls]
    # remove punctuation and special characters
    tweets_no_mentions = [re.sub(r'(@[A-Za-z0-9]+)|([^0-9A-Za-z \t])|(\w+:\/\/\S+)', r' ', tweet) for tweet in tweets_no_numbers]
    #tweets_no_mentions =  [''.join([ch for ch in tweet if ch not in punc]) for tweet in tweets_no_numbers]
    # Create a sublist of lower case words for each tweet
    words_in_tweet = [tweet.lower().split() for tweet in tweets_no_mentions]
    # Remove stop words from each tweet list of words
    tweets_nsw = [[word for word in tweet_words if not word in stop_words]for tweet_words in words_in_tweet]
    # Create list of lists containing bigrams in tweets
    terms_bigram = [list(nltk.bigrams(tweet)) for tweet in tweets_nsw]
    # Flatten list of bigrams in clean tweets
    bigrams = list(itertools.chain(*terms_bigram))
    
    if keywords is not None:
        bigrams = filter_bigrams(keywords, bigrams)
    
    # Create counter of words in clean bigrams
    bigram_counts = collections.Counter(bigrams)

    return bigram_counts.most_common()


def find_entity_occurences(tweet,tag = 'GPE'):
    """
    Return a list of actors from `doc` with corresponding occurences.
    
    :param doc: Spacy NLP parsed list of articles
    :return: list of tuples in form
        [('elizabeth', 622), ('darcy', 312), ('jane', 286), ('bennet', 266)]
    """
    t_txt = None
    if tweet['extended_tweet'] is not None:
        t_txt = nlp(tweet['extended_tweet']['full_text'])
    elif tweet['text'] is not None:
        t_txt = nlp(tweet['text'])
    found_entities = Counter()
    if t_txt is not None:
        for ent in t_txt.ents:
            if ent.label_ == tag:
                found_entities[ent.lemma_] += 1
              
    return found_entities



def get_locations_counts(cursor, start_time=None, end_time=None, keywords=None, disease_name = None):
    '''
    Extract some geographic-related infos from tweets collection, 
    filtering tweets by matching keywords
  
    Args:
    cursor: cursor to a mongo collection with tweets
    start_time: datetime or None, tweets older than this time are retrieved
    end_time: datetime or None, tweets up to this time are retrieved
    keywords: list or None, list of words to filter the bigrams with
    disease_name: str, name one STAMINA disease or None
    
    Returns:
    places: list of places
    countries: list of countries
    places_mentions: list of places mentioned in tweet text
    tweets_with_k: number of tweets containing keywords

    '''
    
    if (start_time is not None) and (end_time is not None):
         cursor_obj = cursor.find( {'created_at': {'$lt': end_time, '$gte': start_time}})
    else:
        cursor_obj = cursor.find()
    
    # make list of all words to look for
    disease_keywords = get_disease_keywords(disease_name)
    keywords = keywords + disease_keywords
    
    places = []
    countries = []
    places_mentions = []
    geolocator = Nominatim(user_agent="STAMINA-h2020")
    tweets_with_k = 0
    for t in cursor_obj:
        if len(search_words_in_tweet(t, keywords))>0:
            # look for mentions of places in the text
            places_ent = list(find_entity_occurences(t,tag = 'GPE'))
            if len(places_ent)>0:
                places_mentions = places_mentions + places_ent
            # look if geo info provided in geo or place fields
            if t['geo'] is not None: 
                g = geolocator.reverse(t['geo']['coordinates'])
                places.append(g.raw['address']['city'])
                countries.append(g.raw['address']['country'])
            elif (t['place'] is not None) and (t['geo'] is None):
                places.append(t['place']['full_name'])
                countries.append(t['place']['country'])   
            else:
                tweets_with_k += 1
    
    return places, countries, places_mentions, tweets_with_k
