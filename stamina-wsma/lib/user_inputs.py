LOCATIONS = ['London','United Kingdom']
DISEASE = 'Covid-19'
KEYWORDS = ['cough','fever']
LANGUAGES = ['en']
FOLLOWS = False